export interface Bookmark{
    id:string;
    name:string;
    link:string;
}