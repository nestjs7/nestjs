import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { Bookmark } from './bookmark.model';
import { BookmarksService } from './bookmarks.service';
import { CreateBookmarkDto } from './dto/create-bookmark.dto';
import { GetBookmarkDto } from './dto/get-bookmark.dto';

@Controller('bookmarks')
export class BookmarksController {
    constructor(private bookmarksservice:BookmarksService){}

    @Get()
    findAll():Bookmark[]{
       return this.bookmarksservice.findAll()
    }

    @Get()
    find(@Query() GetBookmarkDto:GetBookmarkDto):Bookmark[]{
       if (Object.keys(GetBookmarkDto).length) {
           return this.bookmarksservice.findBySearch(GetBookmarkDto);
       }
       return this.bookmarksservice.findAll();
    }

    @Get('/:id')
    findById(@Param('id') id:string):Bookmark{
       return this.bookmarksservice.findById(id)
    }

    @Delete('/:id')
    deleteBookmark(@Param('id') id:string):void{
        return this.bookmarksservice.deleteBookmark(id)
    }

    @Post()
    createBookmark(@Body() CreateBookmarkDto:CreateBookmarkDto ):Bookmark{
        return this.bookmarksservice.createNewBookmark(CreateBookmarkDto)
    }

    //update name 
    @Patch('/:id/name')
    updateBookmark(@Param('id') id:string,@Body('name') name:string):Bookmark{
        return this.bookmarksservice.updateBookmark(id,name)
    }

}
