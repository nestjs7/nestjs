import { Injectable } from '@nestjs/common';
import { Bookmark } from './bookmark.model';
import { v4 as uuid } from 'uuid';
import { CreateBookmarkDto } from './dto/create-bookmark.dto';
import { GetBookmarkDto } from './dto/get-bookmark.dto';


@Injectable()
export class BookmarksService {
    private bookmarks:Bookmark[] = []

    findAll():Bookmark[] {
        return this.bookmarks;
    }

    findById(id):Bookmark{
        return this.bookmarks.find(bookmark=> bookmark.id === id)
    }

    findBySearch(GetBookmarkDto:GetBookmarkDto):Bookmark[]{
        const {name,link} = GetBookmarkDto;
         let bookmarks = this.findAll()
        if (name) {
            bookmarks= this.bookmarks.filter(bookmark => bookmark.name.toLocaleLowerCase().includes(name))
        }
        if (link) {
            bookmarks= this.bookmarks.filter(bookmark => bookmark.link.toLocaleLowerCase().includes(link))
        }
        return bookmarks
    }
    
    deleteBookmark(id:string):void{
        this.bookmarks = this.bookmarks.filter(bookmark => bookmark.id !== id)
    }

    createNewBookmark(CreateBookmarkDto:CreateBookmarkDto):Bookmark{
        const {name,link} = CreateBookmarkDto
        const newBookmarks:Bookmark = {
            id:uuid(),
            name,
            link
        }
        this.bookmarks.push(newBookmarks);
        return newBookmarks
    }
    

    updateBookmark(id:string,name:string):Bookmark{
        const bookmark = this.findById(id);
        bookmark.name = name
        return bookmark
    }

}
